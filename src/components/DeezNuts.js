import React from "react";

export default function DeezNuts(props) {
    return(
        <article className="p-10 shadow-lg">
            <h1 className="text-3xl font-bold">DeezNuts</h1>
            <p>These are nuts, deez nuts. {props.nuts}</p>
        </article>
    )
}